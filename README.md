# Python Week Of Code, Namibia 2019

We want to create a page with a form to create new blog posts.

## Task for Instructor

1. Add

   ```
   path('blog/', views.blog_form, name='blog_form'),
   ```
   
   to [blog/urls.py](blog/urls.py).
2. Add the function

   ```
   def blog_form(request):
       return render(
           request,
           'blog/post_form.html',
       )
   ```

   to [blog/views.py](blog/views.py).
3. Create [blog/templates/blog/post_form.html](blog/templates/blog/post_form.html) and add

   ```
   <!DOCTYPE html>
   <html>
   <head>
   <title>My Site - Blog - New Post</title>
   </head>
   <body>
   <article>
   <h1>New Post</h1>

   <form action="." method="post">
       {% csrf_token %}
       Title:<br>
       <input type="text" name="title"><br>
       Text:<br>
       <input type="text" name="text"><br>
       <input type="submit" value="Submit">
   <form></form>
   </article>
   </body>
   </html>
   ```
4. Open http://localhost:8000/blog/ with your web browser.
5. Fill the form and click on "Submit".

   Note the difference in the log:

   ```
   [12/Aug/2019 21:58:34] "GET /blog/ HTTP/1.1" 200 465
   [12/Aug/2019 21:58:43] "POST /blog/ HTTP/1.1" 200 465
   ```

   Our function didn't have instructions to process the information that we submitted.
6. Change the function `blog_form` to

   ```
   def blog_form(request):
       if request.POST:
           post = Post(
               title=request.POST["title"],
               text=request.POST["text"]
           )
           post.save()
           return redirect('blog', title=post.title)

       return render(
           request,
           'blog/post_form.html',
       )
   ```
7. Open http://localhost:8000/blog/ with your web browser. Fill the form and click on "Submit".

## Tasks for Learners

1. Add a field in the form to save the published date.
2. Change the view to save the published date.